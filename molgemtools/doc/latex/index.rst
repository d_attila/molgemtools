.. molgemtools documentation master file, created by
   sphinx-quickstart on Sat May  9 18:35:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

===========
molgemtools
===========

molgemtools reference
=====================

molgemtools.geom
----------------

.. automodule:: molgemtools.geom

.. autofunction:: molgemtools.geom.open_xyz

.. autofunction:: molgemtools.geom.read_xyz

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.write_xyz

.. autofunction:: molgemtools.geom.write_zmat

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.xyz_from_zmat

.. autofunction:: molgemtools.geom.center

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.rmsd

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.smsvd

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.rmat

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.euler

.. raw:: latex

    \newpage

.. autoclass:: molgemtools.geom.Geom

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.Geom.adjacency

.. autofunction:: molgemtools.geom.Geom.distance

.. autofunction:: molgemtools.geom.Geom.angle

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.Geom.dihedral

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.Geom.eudistmat

.. autofunction:: molgemtools.geom.Geom.conformer

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.Geom.inertia

.. autofunction:: molgemtools.geom.Geom.permute

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.geom.Geom.zmat

.. raw:: latex

    \newpage

molgemtools.graph
-----------------

.. automodule:: molgemtools.graph

.. plot:: alanine_graph.py

    A graph representing the alanine molecule.

.. autoclass:: molgemtools.graph.Graph

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.graph.Graph.adjmat

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.graph.Graph.distmat

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.graph.Graph.vertex

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.graph.Graph.distsum

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.graph.Graph.laplacian

.. raw:: latex

    \newpage

.. autofunction:: molgemtools.graph.Graph.components

.. raw:: latex

    \newpage

molgemtools.constants
---------------------

.. automodule:: molgemtools.constants

.. autoclass:: molgemtools.constants.Constants

Available physical constants:

.. py:data:: molgemtools.constants.Constants.c
    :noindex:

.. py:data:: molgemtools.constants.Constants.a
    :noindex:

.. py:data:: molgemtools.constants.Constants.cal
    :noindex:

.. py:data:: molgemtools.constants.Constants.e_h
    :noindex:

.. py:data:: molgemtools.constants.Constants.h
    :noindex:

.. py:data:: molgemtools.constants.Constants.k
    :noindex:

.. py:data:: molgemtools.constants.Constants.n_a
    :noindex:

.. py:data:: molgemtools.constants.Constants.u
    :noindex:

.. py:data:: molgemtools.constants.Constants.m_e
    :noindex:

.. py:data:: molgemtools.constants.Constants.m_p
    :noindex:

.. py:data:: molgemtools.constants.Constants.ang
    :noindex:

.. py:data:: molgemtools.constants.Constants.e
    :noindex:

.. py:data:: molgemtools.constants.Constants.a_to_ang
    :noindex:

.. py:data:: molgemtools.constants.Constants.m_p_to_m_e
    :noindex:

A dictionary containing the atomic numbers of the first 96 elements:

.. py:data:: molgemtools.constants.Constants.n_dict
    :noindex:

A dictionary containing the average atomic masses of the first 96 elements:

.. py:data:: molgemtools.constants.Constants.m_dict
    :noindex:

A dictionary containing the covalent radii of the first 96 elements:

.. py:data:: molgemtools.constants.Constants.r_dict
    :noindex:

.. include:: constants.rst