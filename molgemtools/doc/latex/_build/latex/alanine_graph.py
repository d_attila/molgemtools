import matplotlib
import matplotlib.pyplot as plt
import os
import sys
sys.path.insert(0, os.path.abspath("../../../../molgemtools"))
import molgemtools.geom as mg


path = "../../../../molgemtools/molgemtools/data/alanine/conformers/ala_1.xyz"
g = mg.Geom(mg.open_xyz(path))
a = g.atoms
adj = g.adjacency()
colors = {"C": "#000000", "H": "#7f7f7f", "O": "#d62728", "N": "#1f77b4"}
x = [2, 1, 1, 3, 3, 4, 0, 1, 0, 2, 0, 1, 2]
y = [2, 2, 1, 3, 1, 3, 2, 3, 0, 0, 4, 4, 4]
xp = []
yp = []
for i in range(g.n):
    for j in adj[i]:
        xp.append(x[i])
        yp.append(y[i])
        xp.append(x[j])
        yp.append(y[j])
        xp.append(None)
        yp.append(None)
font = {"family": "serif",
        "serif": ["Computer Modern"],
        "size": 9}
matplotlib.rc("font", **font)
matplotlib.rc("text", usetex=True)
plt.figure(figsize=[5.0, 2.5])
plt.axes(frameon=False)
plt.xticks([])
plt.yticks([])
plt.axis("equal")
for i in range(g.n):
    plt.scatter(x[i], y[i], s=120, zorder=1, color=colors[a[i]])
    plt.text(x[i], y[i], i, verticalalignment="center",
             horizontalalignment="center", color="#ffffff")
plt.rcParams["savefig.transparent"] = True
plt.plot(xp, yp, color="#000000", zorder=0)
