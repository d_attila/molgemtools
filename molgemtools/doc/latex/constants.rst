.. toctree::
    :maxdepth: 2
    :caption: Contents:

.. raw:: latex

    \newpage
    \begin{table}[!h]
    \caption{Fundamental constants and relevant units in physics and chemistry.}
    \begin{tabular*}{\textwidth}{m{0.5\textwidth}
                                 m{0.15\textwidth}
                                 m{0.25\textwidth}}
    \hline
    Description                                & Name               & Value                           \\
    \hline
    Speed of light in vacuum [$\frac{m}{s}$]   & c                  & $2.99792458\cdot 10^{8}$        \\
    Bohr radius [$m$]                          & a                  & $5.2917721067\cdot 10^{-11}$    \\
    Thermochemical calorie [$J$]               & cal                & 4.184                           \\
    Hartree energy [$J$]                       & e\_h               & $4.359744650\cdot 10^{-18}$     \\
    Planck constant [$J\cdot s$]               & h                  & $6.62607015\cdot 10^{-34}$      \\
    Boltzmann constant [$\frac{J}{K}$]         & k                  & $1.380649\cdot 10^{-23}$        \\
    Avogadro constant [$\frac{1}{mol}$]        & n\_a               & $6.02214076\cdot 10^{23}$       \\
    Atomic mass unit, amu [$kg$]               & u                  & $1.660539040\cdot 10^{-27}$     \\
    Electron mass [$kg$]                       & m\_e               & $9.10938356\cdot 10^{-31}$      \\
    Proton mass [$kg$]                         & m\_p               & $1.672621898\cdot 10^{-27}$     \\
    Angstrom [$m$]                             & ang                & $10^{-10}$                      \\
    Elementary charge [$C$]                    & e                  & $1.6021766208\cdot 10^{-19}$    \\
    Bohr radius to angstrom conversion ratio   & a\_to\_ang         & 0.52917721067                   \\
    Proton-electron mass ratio                 & m\_p\_to\_m\_e     & $1.83615267389\cdot 10^{3}$     \\
    \hline
    \end{tabular*}
    \end{table}

.. raw:: latex

    \newpage
    \begin{table}[!h]
    \centering
    \caption{Data related to chemical elements from H to Cd.}
    \begin{tabular*}{\textwidth}{m{0.25\textwidth}
                                 m{0.25\textwidth}
                                 m{0.25\textwidth}
                                 m{0.25\textwidth}}
    \hline
    Chemical symbol & Atomic number & Average atomic mass \newline [$u$] & Covalent radius \newline [$angstrom$] \\
    \hline
    H               & 1             & 1.007947                   & 0.31 \\
    He              & 2             & 4.0026022                  & 0.28 \\
    Li              & 3             & 6.9412                     & 1.28 \\
    Be              & 4             & 9.0121823                  & 0.96 \\
    B               & 5             & 10.8117                    & 0.84 \\
    C               & 6             & 12.01078                   & 0.76 \\
    N               & 7             & 14.00672                   & 0.71 \\
    O               & 8             & 15.99943                   & 0.66 \\
    F               & 9             & 18.99840325                & 0.57 \\
    Ne              & 10            & 20.17976                   & 0.58 \\
    Na              & 11            & 22.989769282               & 1.66 \\
    Mg              & 12            & 24.30506                   & 1.41 \\
    Al              & 13            & 26.98153868                & 1.21 \\
    Si              & 14            & 28.08553                   & 1.11 \\
    P               & 15            & 30.9737622                 & 1.07 \\
    S               & 16            & 32.0655                    & 1.05 \\
    Cl              & 17            & 35.4532                    & 1.02 \\
    Ar              & 18            & 39.9481                    & 1.06 \\
    K               & 19            & 39.09831                   & 2.03 \\
    Ca              & 20            & 40.0784                    & 1.76 \\
    Sc              & 21            & 44.9559126                 & 1.70 \\
    Ti              & 22            & 47.8671                    & 1.60 \\
    V               & 23            & 50.94151                   & 1.53 \\
    Cr              & 24            & 51.99616                   & 1.39 \\
    Mn              & 25            & 54.9380455                 & 1.61 \\
    Fe              & 26            & 55.8452                    & 1.52 \\
    Co              & 27            & 58.9331955                 & 1.50 \\
    Ni              & 28            & 58.69344                   & 1.24 \\
    Cu              & 29            & 63.5463                    & 1.32 \\
    Zn              & 30            & 65.382                     & 1.22 \\
    Ga              & 31            & 69.7231                    & 1.22 \\
    Ge              & 32            & 72.641                     & 1.20 \\
    As              & 33            & 74.921602                  & 1.19 \\
    Se              & 34            & 78.963                     & 1.20 \\
    Br              & 35            & 79.9041                    & 1.20 \\
    Kr              & 36            & 83.7982                    & 1.16 \\
    Rb              & 37            & 85.46783                   & 2.20 \\
    Sr              & 38            & 87.621                     & 1.95 \\
    Y               & 39            & 88.905852                  & 1.90 \\
    Zr              & 40            & 91.2242                    & 1.75 \\
    Nb              & 41            & 92.906382                  & 1.64 \\
    Mo              & 42            & 95.962                     & 1.54 \\
    Tc              & 43            & 97.9072                    & 1.47 \\
    Ru              & 44            & 101.072                    & 1.46 \\
    Rh              & 45            & 102.905502                 & 1.42 \\
    Pd              & 46            & 106.421                    & 1.39 \\
    Ag              & 47            & 107.86822                  & 1.45 \\
    Cd              & 48            & 112.4118                   & 1.44 \\
    \hline
    \end{tabular*}
    \end{table}

.. raw:: latex

    \newpage
    \begin{table}[!h]
    \centering
    \caption{Data related to chemical elements from In to Cm.}
    \begin{tabular*}{\textwidth}{m{0.25\textwidth}
                                 m{0.25\textwidth}
                                 m{0.25\textwidth}
                                 m{0.25\textwidth}}
    \hline
    Chemical symbol & Atomic number & Average atomic mass \newline [$u$] & Covalent radius \newline [$angstrom$] \\
    \hline
    In              & 49            & 114.8183                   & 1.42 \\
    Sn              & 50            & 118.7107                   & 1.39 \\
    Sb              & 51            & 121.7601                   & 1.39 \\
    Te              & 52            & 127.603                    & 1.38 \\
    I               & 53            & 126.904473                 & 1.39 \\
    Xe              & 54            & 131.2936                   & 1.40 \\
    Cs              & 55            & 132.90545192               & 2.44 \\
    Ba              & 56            & 137.3277                   & 2.15 \\
    La              & 57            & 138.905477                 & 2.07 \\
    Ce              & 58            & 140.1161                   & 2.04 \\
    Pr              & 59            & 140.907652                 & 2.03 \\
    Nd              & 60            & 144.2423                   & 2.01 \\
    Pm              & 61            & 144.9127                   & 1.99 \\
    Sm              & 62            & 150.362                    & 1.98 \\
    Eu              & 63            & 151.9641                   & 1.98 \\
    Gd              & 64            & 157.253                    & 1.96 \\
    Tb              & 65            & 158.925352                 & 1.94 \\
    Dy              & 66            & 162.5001                   & 1.92 \\
    Ho              & 67            & 164.930322                 & 1.92 \\
    Er              & 68            & 167.2593                   & 1.89 \\
    Tm              & 69            & 168.934212                 & 1.90 \\
    Yb              & 70            & 173.0545                   & 1.87 \\
    Lu              & 71            & 174.96681                  & 1.87 \\
    Hf              & 72            & 178.492                    & 1.75 \\
    Ta              & 73            & 180.947882                 & 1.70 \\
    W               & 74            & 183.841                    & 1.62 \\
    Re              & 75            & 186.2071                   & 1.51 \\
    Os              & 76            & 190.233                    & 1.44 \\
    Ir              & 77            & 192.2173                   & 1.41 \\
    Pt              & 78            & 195.0849                   & 1.36 \\
    Au              & 79            & 196.9665694                & 1.36 \\
    Hg              & 80            & 200.592                    & 1.32 \\
    Tl              & 81            & 204.38332                  & 1.45 \\
    Pb              & 82            & 207.21                     & 1.46 \\
    Bi              & 83            & 208.980401                 & 1.48 \\
    Po              & 84            & 208.9824                   & 1.40 \\
    At              & 85            & 209.9871                   & 1.50 \\
    Rn              & 86            & 222.0176                   & 1.50 \\
    Fr              & 87            & 223.0197                   & 2.60 \\
    Ra              & 88            & 226.0254                   & 2.21 \\
    Ac              & 89            & 227.0277                   & 2.15 \\
    Th              & 90            & 232.038062                 & 2.06 \\
    Pa              & 91            & 231.035882                 & 2.00 \\
    U               & 92            & 238.028913                 & 1.96 \\
    Np              & 93            & 237.0482                   & 1.90 \\
    Pu              & 94            & 244.0642                   & 1.87 \\
    Am              & 95            & 243.0614                   & 1.80 \\
    Cm              & 96            & 247.0704                   & 1.69 \\
    \hline
    \end{tabular*}
    \end{table}