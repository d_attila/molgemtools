"""A script for running the doctests of molgemtools.geom and
molgemtools.graph.
"""


import os
import sys
os.chdir(os.path.dirname(__file__) + "/molgemtools")
sys.path.append(os.path.dirname(__file__))
import doctest
import molgemtools.geom as mg
import molgemtools.graph as mgr


doctest.testmod(mg)
print("Doctest of molgemtools.geom...")
doctest.testmod(mgr)
print("Doctest of molgemtools.graph...")
input()
