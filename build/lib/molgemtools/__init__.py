"""Tools for working with molecular geometry data.
"""


__author__ = "Attila Dékány"
__copyright__ = "2020, Attila Dékány"
__contact__ = "dekanyattilaadam@gmail.com"
__version__ = "0.0.10"
